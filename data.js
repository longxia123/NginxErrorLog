/**
 * Created by shadow on 2017/1/15.
 */
$(function () {
    var formHead = '' +
        '<thead>\
            <tr>\
                <th>Date</th>\
                <th>Request</th>\
                <th>Error</th>\
                <th>IP</th>\
                <th>Host</th>\
                <th>referrer</th>\
            </tr>\
        </thead>';

    loadData = function () {
        $.ajax({
            type: "POST",
            url: "log.php",
            data: "",
            dataType: 'json',
            cache: false,
            success: function (data) {
                var html = '<tbody>';
                for (var x in data) {
                    html += '\
                    <tr class="odd gradeX">\
                        <td>'+data[x].date+'</td>\
                        <td>'+data[x].request+'</td>\
                        <td>'+data[x].error+'</td>\
                        <td>'+data[x].client+'</td>\
                        <td>'+data[x].host+'</td>\
                        <td>'+data[x].referrer+'</td>\
                     </tr>';
                }
                html += '</tbody>';
                $('#table').html(formHead+html);
            }
        });

    };
    loadData();
});